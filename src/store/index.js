import { createStore } from 'vuex'

export default createStore({
  state: {
    stock:[{date:'2021年10月28日',close:152.57,open:149.82,high:153.16,low:149.72,volume:'89.71M',increase:'2.50%'},
           {date:'2021年10月27日',close:148.85,open:149.36,high:149.73,low:148.49,volume:'55.79M',increase:'-0.31%'},
           {date:'2021年10月26日',close:149.32,open:149.33,high:150.84,low:149.01,volume:'60.63M',increase:'0.46%'},
           {date:'2021年10月25日',close:148.64,open:148.68,high:149.37,low:147.62,volume:'50.38M',increase:'-0.03%'},
           {date:'2021年10月22日',close:148.69,open:149.69,high:150.18,low:148.64,volume:'58.88M',increase:'-0.53%'},
           {date:'2021年10月21日',close:152.57,open:149.82,high:153.16,low:149.72,volume:'89.71M',increase:'2.50%'},
           {date:'2021年10月20日',close:149.26,open:148.70,high:149.75,low:148.12,volume:'58.09M',increase:'0.34%'},
           {date:'2021年10月19日',close:148.76,open:147.01,high:149.17,low:146.55,volume:'76.38M',increase:'1.51%'},
           {date:'2021年10月18日',close:146.55,open:143.45,high:146.84,low:143.16,volume:'82.92M',increase:'1.18%'},
           {date:'2021年10月15日',close:144.84,open:143.77,high:144.90,low:143.51,volume:'67.94M',increase:'0.75%'}
          ]
  },
  mutations: {

  },
  actions: {
  },
  getters: {
    stock(state){
      return state.stock
    }
  }
})
